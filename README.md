# README
This project contains a Docker container I put together for the CI bootcamp. The idea is to have an image that already has Jupyter installed so that I can use this image in pipelines that convert Jupyter notebooks to PDFs. Using this image allows me to speed up the process by avoiding re-instaling Jupyter every time the pipeline runs. 
