# https://blog.realkinetic.com/building-minimal-docker-containers-for-python-applications-37d0272c52f3

#FROM python:3.8-alpine
FROM ubuntu:bionic
COPY requirements.txt /
#RUN sed -i 's/$/ universe/' /etc/apt/sources.list
RUN apt-get -y update && apt-get -y --force-yes install blender patch build-essential fortune libxv1 libx11-6 lolcat wget python3.8 python3.8-venv python3-pip  
RUN pip3 install -r /requirements.txt
RUN mkdir -p /opt
RUN wget -O /opt/main.py https://raw.githubusercontent.com/pytorch/examples/master/mnist/main.py
RUN python3 -V

# CMD ["jupyter","nbconvert","*.ipynb"]
